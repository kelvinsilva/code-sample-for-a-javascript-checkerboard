C++ program with a user interface to generate HTML code for a checkerboard. The user will be able to specify a certain color, and size of checkerboard before generating HTML code.

The squares on the checkerboard differ by alternating colors, just like a regular checkers board.

On the bottom on the checkers board, there are two places, where you could enter the names of the 2 players, and a button saying “Start!”

When “Start!” is clicked, it displays the names of the players in different colors on top of the checkers board

When clicking on the empty checker squares on the board, a checker appears (black circle), and remain on the board.

When click on a square where a checker has been previously “placed”, that checker disappears and is replaced with an empty board spot, as it was in the beginning.